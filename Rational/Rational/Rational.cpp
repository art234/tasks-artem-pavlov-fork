﻿// Ration.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;

int NOD(int a,int b){									//возврат наибольшего общего делителя
	while (a > 0 && b > 0) {
		if (a > b) {
			a %= b;
		}
		else {
			b %= a;
		}
	}
	return a + b;
}

class Rational {
public:

	Rational(int a = 1, int b = 1){					//Конструктор класса рацинальных дробей
		denominator = a / NOD(abs(a), abs(b));		//Числитель - целое число, знаменатель натуральное
		divider = b / NOD(abs(a), abs(b));			//Так же проводится сокращение дроби
		if ((a < 0) && (b < 0)) {
			a = -a;
			b = -b;
		}

		if ((b < 0) && (a > 0)) {
			denominator = -a;
			divider = -b;
		}
	}
	
	int ReturnDenominator() const {				//Возврат числителя
		return denominator;
	}

	int ReturnDivider() const {						//Возврат знаменателя
		return divider;
	}

private: 
	int denominator; // числитель
	int divider; // знаменатель
};


// Далее идут операторы над дробями: + - * / += -= *= /= == != > < >= <=
Rational operator+(const Rational& lhs, const Rational& rhs) {
	return Rational(lhs.ReturnDenominator() * rhs.ReturnDivider() + rhs.ReturnDenominator() * lhs.ReturnDivider(), lhs.ReturnDivider() * rhs.ReturnDivider()); 
}

Rational operator-(const Rational& lhs) {
	return Rational(-lhs.ReturnDenominator(), lhs.ReturnDivider());
}

Rational operator-(const Rational& lhs, const Rational& rhs) {
	return Rational(lhs + -rhs);
}

Rational operator*(const Rational& lhs, const Rational& rhs) {
	return Rational(lhs.ReturnDenominator() * rhs.ReturnDenominator(), lhs.ReturnDivider() * rhs.ReturnDivider());
}

Rational operator/(const Rational& lhs, const Rational& rhs) {
	return lhs * Rational(rhs.ReturnDivider(),rhs.ReturnDenominator());
}

void operator+=(Rational& lhs, const Rational& rhs) {
	lhs = lhs + rhs;
}

void operator-=(Rational& lhs, const Rational& rhs) {
	lhs = lhs - rhs;
}

void operator*=(Rational& lhs, const Rational& rhs) {
	lhs = lhs * rhs;
}

void operator/=(Rational& lhs, const Rational& rhs) {
	lhs = lhs / rhs;
}

bool operator==(const Rational& lhs, const Rational& rhs) {
	if ((lhs.ReturnDenominator() == rhs.ReturnDenominator()) && (lhs.ReturnDivider() == rhs.ReturnDivider())) {
		return true;
	}
	else {
		return false;
	}
}

bool operator>(const Rational& lhs, const Rational& rhs) {
	if (lhs.ReturnDenominator() * rhs.ReturnDivider() > lhs.ReturnDivider() * rhs.ReturnDenominator()) {
		return true;
	}
	else {
		return false;
	}
}

bool operator<(const Rational& lhs, const Rational& rhs) {
	Rational null = {0};
	if ( rhs - lhs > null) {
		return true;
	}
	else {
		return false;
	}
}

bool operator<=(const Rational& lhs, const Rational& rhs) {
	if ((lhs < rhs) || (lhs == rhs)) {
		return true;
	}
	else {
		return false;
	}
}

bool operator>=(const Rational& lhs, const Rational& rhs) {
	if ((lhs > rhs) || (lhs == rhs)) {
		return true;
	}
	else {
		return false;
	}
}

bool operator!=(const Rational& lhs, const Rational& rhs) {
	Rational null = { 0 };
	if (lhs - rhs == null) {
		return false;
	}
	else {
		return true;
	}
}


istream& operator>>(istream& stream, Rational& number) {			//оператор ввода в поток числа в виде a/b через cin
	int a, b;
	stream >> a;
	stream.ignore(1);
	stream >> b;
	number = { a, b };
	return stream;
}

ostream& operator<<(ostream& stream, const Rational& number) {		//оператор вывода в поток через cout
	if (number.ReturnDivider() == 1) {
		stream << number.ReturnDenominator();
	}
	else {
		stream << number.ReturnDenominator() << "/" << number.ReturnDivider();
	}
	return stream;
}

int main()
{
	Rational number1;
	cin >> number1;
	Rational number2 = { 27, -14 }; 
	cout << number2 << endl;
	if (number1 != number2) {
		cout  << number1 << " != " << number2 << endl;
	}
	else {
		cout  << number1 << " == " << number2 << endl;
	}
	number1 /= number2;
	cout << number2;
	return 0;
}